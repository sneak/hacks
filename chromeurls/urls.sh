#!/bin/bash

SRC="$HOME/Library/Application Support/Google/Chrome/Default/History"

Q="SELECT
  datetime(last_visit_time/1000000-11644473600, \"unixepoch\") as last_visited,
  last_visit_time as orig,
  url,
  title,
  visit_count
FROM urls
ORDER BY last_visited desc

LIMIT 1
;
"

mkdir -p $HOME/tmp

cp "$SRC" $HOME/tmp/history

sqlite3 -readonly $HOME/tmp/history "$Q"

rm $HOME/tmp/history
