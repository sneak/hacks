#!/bin/bash

DEST="/Volumes/video"
D="root@las1"

function main() {
    if [[ ! -d "$DEST/2022" ]]; then
        echo "wrong box" > /dev/stderr
        exit 1
    fi
    SRC="$1"
    cd "$SRC"
    for FILE in * ; do
        YYYY="$(echo "$FILE" | colrm 5)"
        MM="$(echo "$FILE" | colrm 1 4 | colrm 3)"
        DD="$(echo "$FILE" | colrm 1 6 | colrm 3)"
        echo "ssh $D -- mkdir -p /storage/video/$YYYY/$YYYY-$MM/$YYYY-$MM-$DD"
        echo "rsync -acvvvP \"$SRC/$FILE\" $D:/storage/video/$YYYY/$YYYY-$MM/$YYYY-$MM-$DD/"
    done
}

main $*
