#!/bin/bash

for FIRSTLETTER in $(jot -w %c 26 a); do
    for SECONDLETTER in $(jot -w %c 26 a); do
    ST="$FIRSTLETTER$SECONDLETTER"
    echo "searching for $ST"
curl 'https://www.dnalounge.com/musicvideos/jwztv/search/' \
  -H 'Connection: keep-alive' \
  -H 'sec-ch-ua: "Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36' \
  -H 'sec-ch-ua-platform: "macOS"' \
  -H 'Content-type: text/plain' \
  -H 'Accept: */*' \
  -H 'Origin: https://www.dnalounge.com' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Referer: https://www.dnalounge.com/musicvideos/' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-raw "$ST" \
  --compressed | jq . > $ST.json
    done
done

