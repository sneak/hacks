#!/bin/bash

set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

INPUTFILE="$DIR/all.txt"
DESTDIR="/Volumes/movies/_MUSIC_VIDEOS/jwztv-youtube-scrape"

while IFS= read -r SEARCHTERM; do
    cd $DESTDIR
    yt-dlp "ytsearch:$SEARCHTERM"
done < "$INPUTFILE"
