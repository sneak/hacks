#!/bin/bash


SERVERS="$(curl -sS https://api.mullvad.net/www/relays/all/ |
    jq -jr '.[] | .hostname, ",", .city_code, ",", .type, ",", .provider, "\n"' |
    grep wireguard)"

for LINE in $SERVERS ; do
    hostname="$(echo $LINE | awk -F, '{print $1}')"
    city="$(echo $LINE | awk -F, '{print $2}')"
    provider="$(echo $LINE | awk -F, '{print $4}')"
    short="$(echo $hostname | awk -F'-' '{print $1}')"
    echo mv mullvad-$short.conf $short-$city-$provider.conf
    echo mv $short-wireguard.conf $short-$city-$provider.conf
done
