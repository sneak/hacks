#!/bin/bash

set -x

POOLNAME="tank"
MPT="/srv/z"
JOBFILE="/tmp/jobfile.fio"
KEYLOC="/tmp/testkey.raw"

function main() {
    show_system_info
    do_zfs_tests
}

function print_divider() {
    echo '##############################################################'
    echo '##############################################################'
}

function show_system_info() {
    print_divider
    date -u
    lsb_release -a
    uname -a
    lsblk -t
    nvme list
    modinfo zfs | grep -iw version
    modinfo spl | grep -iw version
    zfs version
    print_divider
}

function prep_system() {
    killall fio || true
    sleep 1

}

function do_zfs_tests() {
        print_divider

        FO="--eta-newline=10 --eta-interval=10 --eta=always"

        for NJ in 1 16 ; do
            for BS in 64k ; do
                for DIRNAME in none comp enc both ; do

cat > $JOBFILE <<EOF

[test-$DIRNAME-${BS}chunk-${NJ}thread]
bs=$BS
directory=$MPT/$DIRNAME/
iodepth=64
ioengine=libaio
readwrite=readwrite
numjobs=$NJ
nrfiles=$NJ
size=10000M
runtime=120
time_based=1
group_reporting=1
EOF
                cat $JOBFILE
                fio $FO $JOBFILE 2>&1
                rm -rf $MPT/$DIRNAME/*
                done
            done
        done

}

main
