#!/bin/bash

# thanks to @sebble for the basis for this script, he did the hard work:
# https://gist.github.com/sebble/e5af3d03700bfd31c62054488bfe8d4f

function list_starred_repos {
    USER=${1:-sneak}

    STARS=$(curl -sI https://api.github.com/users/$USER/starred?per_page=1|egrep '^Link'|egrep -o 'page=[0-9]+'|tail -1|cut -c6-)
    PAGES=$((658/100+1))

    echo You have $STARS starred repositories. > /dev/stderr

    # cloning
    # https://github.com/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    # breaks github's auth system
    REPOS=""
    for PAGE in `seq $PAGES`; do
        REPOS+=" $(
            curl -sH "Accept: application/vnd.github.v3.star+json" \
            "https://api.github.com/users/$USER/starred?per_page=100&page=$PAGE" |
            jq -r '.[]|[.repo.full_name][0]' |
            grep -v eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        )"
    done

    for REPO in $REPOS ; do
        echo "$REPO"
    done
}

function fetch_starred_repos {
    for SHORTNAME in $(list_starred_repos); do
        UN="$(echo $SHORTNAME | cut -d'/' -f1)"
        if [[ ! -d "dl/$SHORTNAME" ]]; then
            if [[ ! -d "dl/$UN" ]]; then
                mkdir -p dl/$UN
            fi
            git clone https://github.com/$SHORTNAME dl/$SHORTNAME
        fi
    done
}

fetch_starred_repos
