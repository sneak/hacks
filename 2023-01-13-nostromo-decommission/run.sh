#!/bin/bash
DEST="$HOME/tmp/2023-01-13-nostromo.oldhome"
mkdir -p "$DEST"
rsync -avP \
		--exclude=/Library/Syncthing \
		--exclude=/Library/Mail \
		--exclude=/Library/Caches \
		--exclude=/Library/Metadata \
		--exclude=/Library/News \
		--exclude=/Library/Python \
		--exclude=/Library/Containers/com.utmapp.UTM \
		--exclude=/Library/Application?Support/Signal \
		--exclude=/.Trash \
		--exclude=/.cache \
		--exclude=/tmp \
		--exclude=/go \
		--exclude=.DS_Store \
		--exclude=/Documents \
		--delete-excluded \
		--delete \
		$HOME/ $DEST/home/

tar -c $DEST/home 2>/dev/null |
	pv --size $(du -sb $DEST/home | awk '{print $1}') |
	zstdmt |
	age -r $SNEAK_LONGTERM_ARCHIVE_AGE_PUB |
	ssh root@lstor1 "cat > /srv/backup/hosts/nostromo/2023-01-13-nostromo-oldhome.tzst.age"
