#!/bin/bash

VAR="ubuntu18.04"
#VAR="generic"

virt-install \
--name $VMNAME \
--cpu host \
--ram 32768 \
--disk path=$DISK_DIR/$VMNAME.qcow2,format=qcow2,size=1000,cache=writeback \
--vcpus 16 \
--os-type linux \
--os-variant "$VAR" \
--network bridge=virbr0 \
--graphics none \
--console pty,target_type=serial \
--initrd-inject=preseed.cfg --extra-args "ks=file:/preseed.cfg"  \
--location 'http://us.archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/' \
--extra-args 'console=ttyS0,115200n8 serial'

