#!/usr/bin/env python3
import serial
from sanelogging import log
import binascii

class Brainduino(object):
    def __init__(self):
        self.inbuffer = bytes()

    def add_bytes(self,bytes):
        self.inbuffer = self.inbuffer + bytes
        if self.buffer_contains_line():
            self.read_and_process_line_from_buffer()

    def buffer_contains_line(self):
        return self.inbuffer.find(b'\r') != -1

    def process_line(self,line):
        line = line.decode("UTF-8")
        if len(line) != 16:
            # this line is incomplete, skip
            return
        log.info("got line: " + line)
        p = line.strip().split("\t")
        print(binascii.unhexlify(p[0] + p[1]))
        print(binascii.unhexlify(p[2] + p[3]))

    def read_and_process_line_from_buffer(self):
        i = self.inbuffer.find(b'\r')
        if i < 1:
            log.error("should not happen")
            return
        line = self.inbuffer[0:i+1]
        self.inbuffer = self.inbuffer[i+1:]
        self.process_line(line)

def main():
    e = Brainduino()

    with serial.Serial(
        '/dev/tty.Alpheratz-SPPDev',
        115200,
        timeout=1
    ) as ser:
        while True:
            e.add_bytes(ser.read(16))


if __name__ == "__main__":
    main()
