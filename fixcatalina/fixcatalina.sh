#!/bin/bash

# script originally found at 
# https://gist.github.com/pwnsdx/1217727ca57de2dd2a372afdd7a0fc21
# thanks to @pwnsdx

# IMPORTANT: Don't forget to logout from your Apple ID in the settings before running it!
# IMPORTANT: You will need to run this script from Recovery. In fact, macOS Catalina brings read-only filesystem which prevent this script from working from the main OS.
# This script needs to be run from the volume you wish to use.
# E.g. run it like this: cd /Volumes/Macintosh\ HD && sh /Volumes/Macintosh\ HD/Users/sabri/Desktop/disable.sh
# WARNING: It might disable things that you may not like. Please double check the services in the TODISABLE vars.

# Get active services: launchctl list | grep -v "\-\t0"
# Find a service: grep -lR [service] /System/Library/Launch* /Library/Launch* ~/Library/LaunchAgents

# Agents to disable
# 'com.apple.speech.speechdatainstallerd' 'com.apple.speech.speechsynthesisd' 'com.apple.speech.synthesisserver' will freeze Edit menus
# 'com.apple.bird' will prevent saving prompt from being shown
TODISABLE=()

# iCloud
TODISABLE+=( \
	'com.apple.cloudd' \
	'com.apple.cloudpaird' \
	'com.apple.cloudphotosd' \
	'com.apple.followupd' \
	'com.apple.iCloudUserNotifications' \
	'com.apple.icloud.findmydeviced.findmydevice-user-agent' \
	'com.apple.icloud.fmfd' \
	'com.apple.icloud.searchpartyuseragent' \
	'com.apple.protectedcloudstorage.protectedcloudkeysyncing' \
    'com.apple.security.cloudkeychainproxy3' \
)

# Safari useless stuff
TODISABLE+=( \
	'com.apple.SafariCloudHistoryPushAgent' \
	'com.apple.WebKit.PluginAgent' \
    'com.apple.SafariBookmarksSyncAgent' \
)

# iMessage / Facetime
TODISABLE+=( \
	'com.apple.avconferenced' \
	'com.apple.imautomatichistorydeletionagent' \
	'com.apple.imklaunchagent' \
	'com.apple.imtransferagent' \
    'com.apple.imagent' \
)

# Game Center / Passbook / Apple TV / Homekit...
TODISABLE+=( \
	'com.apple.CommCenter-osx' \
	'com.apple.Maps.pushdaemon' \
	'com.apple.homed' \
	'com.apple.passd' \
	'com.apple.videosubscriptionsd' \
    'com.apple.gamed' \
)

# Ad-related
TODISABLE+=( \
	'com.apple.ap.adservicesd' \
    'com.apple.ap.adprivacyd' \
)

# Screensharing
TODISABLE+=( \
	'com.apple.screensharing.agent' \
	'com.apple.screensharing.menuextra' \
    'com.apple.screensharing.MessagesAgent' \
)

# Siri
TODISABLE+=( \
	'com.apple.Siri.agent' \
	'com.apple.assistant_service' \
	'com.apple.assistantd' \
	'com.apple.parsec-fbf' \
    'com.apple.siriknowledged' \
)

# VoiceOver / accessibility-related stuff
TODISABLE+=( \
	'com.apple.accessibility.AXVisualSupportAgent' \
	'com.apple.accessibility.dfrhud' \
	'com.apple.accessibility.heard' \
	'com.apple.voicememod' \
    'com.apple.VoiceOver' \
)

# Quicklook
TODISABLE+=( \
	'com.apple.quicklook' \
	'com.apple.quicklook.ThumbnailsAgent' \
    'com.apple.quicklook.ui.helper' \
)

# Sidecar
TODISABLE+=( \
	'com.apple.sidecar-relay'
    'com.apple.sidecar-hid-relay' \
)

# Debugging process
TODISABLE+=( \
	'com.apple.DiagnosticReportCleanup' \
	'com.apple.ReportCrash' \
	'com.apple.ReportGPURestart' \
	'com.apple.ReportPanic' \
	'com.apple.TrustEvaluationAgent' \
    'com.apple.spindump_agent' \
)

# Screentime
TODISABLE+=( \
	'com.apple.UsageTrackingAgent' \
    'com.apple.ScreenTimeAgent' \
)

# Others
TODISABLE+=( \
	'com.apple.AOSHeartbeat' \
	'com.apple.AOSPushRelay' \
	'com.apple.AddressBook.SourceSync' \
	'com.apple.AddressBook.abd' \
	'com.apple.AirPlayUIAgent' \
	'com.apple.AirPortBaseStationAgent' \
	'com.apple.CalendarAgent' \
	'com.apple.CallHistoryPluginHelper' \
	'com.apple.CallHistorySyncHelper' \
	'com.apple.KeyboardAccessAgent' \
	'com.apple.RemoteDesktop' \
	'com.apple.SocialPushAgent' \
	'com.apple.amp.mediasharingd' \
	'com.apple.appleseed.seedusaged' \
	'com.apple.appleseed.seedusaged.postinstall' \
	'com.apple.exchange.exchangesyncd' \
	'com.apple.familycircled' \
	'com.apple.familycontrols.useragent' \
	'com.apple.familynotificationd' \
	'com.apple.findmymacmessenger' \
	'com.apple.helpd' \
	'com.apple.identityservicesd' \
	'com.apple.java.InstallOnDemand' \
	'com.apple.keyboardservicesd' \
	'com.apple.knowledge-agent' \
	'com.apple.macos.studentd' \
	'com.apple.mediaanalysisd' \
	'com.apple.mediaremoteagent' \
	'com.apple.mobileassetd' \
	'com.apple.parentalcontrols.check' \
	'com.apple.parsecd' \
	'com.apple.photoanalysisd' \
	'com.apple.remindd' \
	'com.apple.security.keychain-circle-notification' \
	'com.apple.sharingd' \
	'com.apple.suggestd' \
	'com.apple.syncdefaultsd' \
	'com.apple.telephonyutilities.callservicesd' \
	'com.apple.touristd' \
    'com.apple.telephonyutilities.callservicesd' \
)

echo Disabling unwanted LaunchAgents...
for agent in "${TODISABLE[@]}"; do
    if [[ -e "./System/Library/launchAgents/${agent}.plist" ]]; then
        mv "./System/Library/LaunchAgents/${agent}.plist" \
                "./System/Library/LaunchAgents/${agent}.plist.bak"
        echo "[OK] Agent ${agent} disabled"
    fi
done

# Daemons to disable
TODISABLE=()

# iCloud
TODISABLE+=( \
    'com.apple.analyticsd' \
    'com.apple.icloud.findmydeviced' \
)

# Others
TODISABLE+=( \
	'com.apple.ManagedClient' \
	'com.apple.ManagedClient.cloudconfigurationd' \
	'com.apple.ManagedClient.enroll' \
	'com.apple.ManagedClient.startup' \
	'com.apple.RemoteDesktop.PrivilegeProxy' \
	'com.apple.SubmitDiagInfo' \
	'com.apple.appleseed.fbahelperd' \
	'com.apple.apsd' \
	'com.apple.eapolcfg_auth' \
	'com.apple.locate' \
	'com.apple.locationd' \
	'com.apple.mediaremoted' \
	'com.apple.preferences.timezone.admintool' \
	'com.apple.remotepairtool' \
	'com.apple.screensharing' \
	'com.apple.security.FDERecoveryAgent' \
    'com.apple.netbiosd' \
)

for daemon in "${TODISABLE[@]}" ; do
    if [[ -e "./System/Library/LaunchDaemons/${daemon}.plist" ]]; then
        mv "./System/Library/LaunchDaemons/${daemon}.plist" \
            "./System/Library/LaunchDaemons/${daemon}.plist.bak"
        echo "[OK] Daemon ${daemon} disabled"
    fi
done
