#!/bin/bash

for FN in *.MOV *.MP4 *.MTS *.mov *.mp4; do
    MTIME="$(stat -f "%Sm" "$FN")"
    # eg "Sep 13 05:02:26 2019"
    NP="$(date -j -f "%b %d %T %Y" "$MTIME" "+%Y/%Y-%m/%Y-%m-%d")"
    if [[ -e "$FN" ]]; then
        mkdir -p "$NP"
        mv "$FN" "$NP"
    fi
done
