#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

SD="/var/umbra"

function checkMounted() {
	if [[ ! -e $SD/cur ]]; then
		echo "umbraSync directory not mounted." > /dev/stderr
		exit 2
	fi
}

function writeStats() {
	checkMounted
	OD="$SD/host/$(hostname -s)/stat"
	if [[ ! -d "$OD" ]]; then
		mkdir -p "$OD"
	fi
    ifconfig > "$OD/ifconfig"
    netstat -in > "$OD/ifaces"
    date -u "+%s" > "$OD/cur"
}

function errorFileName () {
	echo "$(date -u +"%Y-%m-%dT%H%M%SZ").$(hostname -s).$(uuidgen).error.txt"
}

function logError() {
	checkMounted
	OD="$SD/host/$(hostname -s)/error"
	if [[ ! -d "$OD" ]]; then
		mkdir -p "$OD"
	fi
	cat > "$OD/$(errorFileName)" <<< "$@"
}

