TARGET := ./berlin.sneak.fs.NNNY-cyberdyne-backup-01

default: backup

backup: do_file_backup write_checksum

write_checksum:
	cd $(TARGET)/fs && find . -type f -print0 | xargs -0 sha1sum > ../.SHASUMS.tmp
	mv ./.SHASUMS.tmp ./$(TARGET)/SHASUMS.txt

do_file_backup:
	rsync -avv \
		--exclude=/tmp \
		--exclude=/.cache \
		--exclude=/.nvm \
		--exclude=/.Trash \
		--exclude=/Library/Caches \
		--exclude=/Library/Mail \
		--exclude=/Library/Developer \
		--exclude=.DS_Store \
		--delete-before \
		--delete-excluded \
		$(HOME)/ $(TARGET)/fs/ 2>&1 | tee -a $(TARGET)/$(shell date -u +%Y-%m-%d).log
		echo '# $(shell date -u)' > $(TARGET)/lastbackup.txt
		date -u '+%s' >> $(TARGET)/lastbackup.txt
