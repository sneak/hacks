THIS = $(shell basename $(CURDIR))
YYYYMMDD = $(shell date -u +%Y-%m-%d)

default: backup

save_makefile:
	cp ./Makefile $(HOME)/dev/hacks/makefiles/$(YYYYMMDD).$(THIS).makefile

backup: save_makefile do_file_backup write_checksum

write_checksum:
	cd ./fs && find . -type f -print0 | xargs -0 sha1sum | tee -a  ../SHASUMS.txt

do_file_backup:
	rsync -avv \
		--exclude=/tmp \
		--exclude=/.cache \
		--exclude=/.nvm \
		--exclude=/.Trash \
		--exclude=/Library/Caches \
		--exclude=/Library/Mail \
		--exclude=/Library/Developer \
		--exclude=.DS_Store \
		--delete-before \
		--delete-excluded \
		$(HOME)/ ./fs/ 2>&1 | tee -a ./$(shell date -u +%Y-%m-%d).log
		echo '# $(shell date -u)' > ./lastbackup.txt
		date -u '+%s' >> ./lastbackup.txt
