THIS = $(shell basename $(CURDIR))
YYYYMMDD = $(shell date -u +%Y-%m-%d)
YYYY = $(shell date +%Y)

default: save_makefile update-lifeboat update-photos

save_makefile:
	cp ./Makefile $(HOME)/dev/hacks/makefiles/$(YYYYMMDD).$(THIS).makefile

update-lifeboat:
	rsync -avP --delete $(HOME)/Library/Syncthing/folders/lifeboat/*.zip /Volumes/1tb-lifeboat1G/berlin.sneak.fs.lifeboat/
	zip -T /Volumes/1tb-lifeboat1G/berlin.sneak.fs.lifeboat/*.zip

update-photos:
	rsync -avP --delete $(HOME)/Library/Syncthing/folders/LightroomMasters-CurrentYear/ ./fs/ 2>&1 | tee -a log.txt
	cd ./fs && find . -type f -print0 | xargs -0 shasum 2&>1 | tee -a ../SHASUMS.txt
