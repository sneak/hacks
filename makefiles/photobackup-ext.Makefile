default: sync

sync:
	rsync -avP \
		--delete \
		--delete-before \
		--delete-excluded \
		--exclude '.sync-conflict*DS_Store' \
		--exclude '.DS_Store' \
		$(HOME)/Library/Syncthing/folders/LightroomMasters-CurrentYear/ \
		./LightroomMasters-CurrentYear/
