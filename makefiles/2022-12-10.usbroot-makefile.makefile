THIS = usbroot-makefile
YYYYMMDD = $(shell date -u +%Y-%m-%d)

default: save_makefile alldirs

save_makefile:
	cp ./Makefile $(HOME)/dev/hacks/makefiles/$(YYYYMMDD).$(THIS).makefile

alldirs:
	for D in ./berlin.sneak.fs.* ; do cd $$D && make && cd .. ; done
