HOMEEXCLUDE := --exclude /.Trash \
		--exclude .DS_Store \
		--exclude /.Spotlight-V100 \
		--exclude /.cache \
		--exclude /.fseventsd \
		--exclude /Library/Caches \
		--exclude /Library/Mail \
		--exclude /Library/Metadata/CoreSpotlight \
		--exclude /tmp

ROOTEXCLUDE := --exclude /.Trash \
    --exclude /proc \
    --exclude /dev \
    --exclude /.fseventsd \
	--exclude .DS_Store \
    --exclude /System/Volumes/Data/Volumes \
    --exclude /private/var/vm \
    --exclude /System/Volumes/Data/nix \
    --exclude /System/Volumes/Data/Users/sneak \
    --exclude /Users/sneak \
    --exclude /System/Volumes/Data/.fseventsd \
    --exclude /System/Volumes/Data/.Spotlight-V100 \
    --exclude /System/Volumes/Data/private/var/folders \
    --exclude /private/var/folders \
    --exclude /Volumes

OPTS := -avP --delete --delete-excluded --delete-before

HOMEOPTS := $(OPTS) $(HOMEEXCLUDE)
ROOTOPTS := $(OPTS) $(ROOTEXCLUDE)

default:
	sudo bash -c 'make synchome; make syncroot'

synchome:
	rsync $(HOMEOPTS) $(HOME)/ ./2021-01-12.nostromo.sneakhome/ | tee -a $(shell date +%Y-%m-%d).homesync.log

syncroot:
	rsync $(ROOTOPTS) / ./2021-01-12.nostromo.root/ | tee -a $(shell date +%Y-%m-%d).rootsync.log

