#!/bin/bash

#-c:v libx264 \
#-c:v libx265 \
#-vf "scale=(iw*sar)*max($W/(iw*sar)\,$H/ih):ih*max($W/(iw*sar)\,$H/ih), crop=$W:$H" \
#-s:v ${W}x${H} \
W="3840"
H="2160"

DIR1="10621108"
DIR2="10721108"

#-vcodec libx264 \
#-crf 17 \
#-preset slow \

function main {
    convertRaws
    makeBigVersion
    makeSmallVersion
}

function makeBigVersion {
    #-c:v hevc_videotoolbox -q:v 100 \
    #-vf scale=-2:${H} \
    ffmpeg -framerate 24 \
        -pattern_type glob \
        -i 'output/frames/*.png' \
        -c:v prores -profile:v 3 \
        -pix_fmt yuv422p10 \
        output/big.mov
}

function makeSmallVersion {
    ffmpeg -i output/big.mov \
        -vf scale=-2:1080 \
        -c:v h264_videotoolbox \
        -b:v 12000K \
        output/preview.1080p.mp4
}

function convertRaws {
    if [[ -e output/frames/done ]]; then
        return
    fi
    mkdir -p output/frames
    rm -rf tmp.sh
    for i in $DIR1/*.ARW; do
        BN="$(basename ${i%.*})"
        echo sips -s format png $i --out "output/frames/1${BN}.png"
        echo sips -s format png $i --out "output/frames/1${BN}.png" >> tmp.sh
    done
    for i in $DIR2/*.ARW; do
        BN="$(basename ${i%.*})"
        echo sips -s format png $i --out "output/frames/2${BN}.png"
        echo sips -s format png $i --out "output/frames/2${BN}.png" >> tmp.sh
    done
    parallel --eta -j $(nproc) bash -c < tmp.sh && touch output/frames/done
}

main
