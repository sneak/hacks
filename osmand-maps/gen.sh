#!/bin/bash

set -x

export YYYYMMDD="$(date -u +%Y-%m-%d)"
export CONTINENTS="
    europe
    northamerica
    southamerica
    centralamerica
    asia
    africa
    oceania
    antarctica
    world
    voice
"

#https://download.osmand.net/download?standard=yes&file=$FN

#https://download.osmand.net/indexes.php

function fetchContinent() {
    CONTINENT="$1"
    cd /work/download
    if [[ ! -d ./$CONTINENT ]]; then
        mkdir $CONTINENT
    fi
    cd $CONTINENT
    FILES="$(grep -i $CONTINENT ../listing.txt)"

    for FILE in $FILES ; do
        URL="https://download.osmand.net/download?standard=yes&file=$FILE"
        if [[ ! -e "$FILE" ]]; then
            echo "file $FILE is missing, downloading"
            wget --progress=dot:giga --report-speed=bits \
                -O "$FILE.tmp" \
                -c \
                "$URL" && mv "$FILE.tmp" "$FILE"
            rm *.tmp
        fi
        ls -tla
        du -sh .
        df -h .
    done
}

function zipContinent() {
    CONTINENT="$1"
    cd /work/download
    du -sh $CONTINENT
    df -h .
    cd $CONTINENT
    find . -type f -iname '*.zip' -print0 | xargs -0 -n 1 -P 8 unzip
    rm -fv *.zip *.tmp
    cd ..
    zip -9r $YYYYMMDD.$CONTINENT.zip $CONTINENT
    rm -rfv $CONTINENT
    mv $YYYYMMDD.$CONTINENT.zip /work/webroot

}

function fetch() {

    cd /work/download

    # srtmf files are 404
    curl -sf https://download.osmand.net/indexes.php | tr "\"" "\n" |
        tr ">" "\n" | tr "<" "\n" | grep obf.zip |
        grep -v "srtmf" | sort > listing.txt

    cat > /work/webroot/index.html.new <<EOF
    <html>
    <head>
    <title>files</title>
    </head>
    <body>
    <h1>files</h1>
    <ul>
EOF

    for CONTINENT in $CONTINENTS; do
        if [[ ! -e /work/webroot/$YYYYMMDD.$CONTINENT.zip ]]; then
            fetchContinent "$CONTINENT"
            zipContinent "$CONTINENT"
        fi

        cat >> /work/webroot/index.html.new <<EOF
<li>
    <a href="/$YYYYMMDD.$CONTINENT.zip">$YYYYMMDD.$CONTINENT.zip</a>
</li>
EOF


    done

cat >> /work/webroot/index.html.new <<EOF
</ul>
<pre>
$(date -u)
</pre>
</body>
</html>
EOF

mv /work/webroot/index.html.new /work/webroot/index.html

}

function main() {
    fetch
}

main
