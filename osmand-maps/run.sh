#!/bin/bash

chmod a+rx /work/bin/*

bash -c "/work/bin/gen.sh" &

rm -rfv /var/www/html
ln -s /work/webroot /var/www/html
chmod -Rv a+rx /work/webroot/*
exec nginx -g "daemon off;"
