#!/bin/bash

set -x

RAWDISKS=$(
    cd /dev/disk/by-id ;
    ls -1 nvme-SAMSUNG* | grep -v part
)

FULLPATHS=""
for RAWDISK in $RAWDISKS ; do
    FULLPATHS+=" /dev/disk/by-id/$RAWDISK"
done

POOLNAME="tank"
MPT="/srv/z"
JOBFILE="/tmp/jobfile.fio"
KEYLOC="/tmp/testkey.raw"

function main() {
    show_system_info
    prep_system
    do_direct_tests
    do_zfs_tests
}

function do_direct_tests() {
    trim_disks
    for DISK in $FULLPATHS; do
        print_divider
        echo "testing $DISK"
        for NJ in 1 16; do
            for BS in 64k ; do
cat > $JOBFILE <<EOF
[test-${BS}chunk-${NJ}thread]
bs=$BS
filename=$DISK
iodepth=64
ioengine=libaio
readwrite=readwrite
numjobs=$NJ
size=$(expr 10000 / $NJ)M
runtime=60
time_based=1
group_reporting=1
EOF
                cat $JOBFILE
                fio $FO $JOBFILE 2>&1
            done
        done
    done
}

function print_divider() {
    echo '##############################################################'
    echo '##############################################################'
}

function trim_disks() {
    for DISK in $FULLPATHS ; do
        blkdiscard $DISK
        nvme format -r $DISK
    done
    print_divider
    echo "################## waiting 60s for nvme format to complete..."
    sleep 60
    nvme list
}


function show_system_info() {
    print_divider
    date -u
    lsb_release -a
    uname -a
    lsblk -t
    nvme list
    modinfo zfs | grep -iw version
    modinfo spl | grep -iw version
    zfs version
    print_divider
}

function prep_system() {
    killall fio || true
    sleep 1

    if [[ -e $MPT/.zfs ]]; then
        zpool destroy $POOLNAME
    fi
}

function do_zfs_tests() {
    for ASHIFTVAL in 12 13 14 15 16 ; do

        trim_disks

        print_divider

        zpool create $POOLNAME \
            -o ashift=$ASHIFTVAL -O mountpoint=$MPT \
            raidz1 $FULLPATHS

        dd if=/dev/urandom of=$KEYLOC bs=32 count=1

        COMP="
            -o compression=lz4
        "
        ENC="
            -o encryption=aes-256-gcm
            -o keyformat=raw
            -o keylocation=file://$KEYLOC
        "

        # plain
        zfs create $POOLNAME/none
        # compressed
        zfs create $COMP $POOLNAME/comp
        # encrypted
        zfs create $ENC $POOLNAME/enc
        # compressed and encrypted
        zfs create $COMP $ENC $POOLNAME/both

        FO="--eta-newline=10 --eta-interval=10 --eta=always"

        for NJ in 1 16 ; do
            for BS in 64k ; do
                for DIRNAME in none comp enc both ; do

cat > $JOBFILE <<EOF

[test-$DIRNAME-${BS}chunk-${NJ}thread-ashift${ASHIFTVAL}]
bs=$BS
directory=$MPT/$DIRNAME/
iodepth=64
ioengine=libaio
readwrite=readwrite
numjobs=$NJ
nrfiles=$NJ
size=10000M
runtime=120
time_based=1
group_reporting=1
EOF
                cat $JOBFILE
                fio $FO $JOBFILE 2>&1
                rm -rf $MPT/$DIRNAME/*
                done
            done
        done

        zpool destroy $POOLNAME
    done
}

main
