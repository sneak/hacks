#!/usr/bin/env python3
from twitterscraper import query_tweets

def main():
    list_of_tweets = query_tweets("@sneak_announce", 10)

    #print the retrieved tweets to the screen:
    for tweet in list_of_tweets:
        print(tweet)

    #Or save the retrieved tweets to file:
    #file = open("output.txt","w")
    #for tweet in query_tweets("Trump OR Clinton", 10):
        #file.write(str(tweet.text.encode('utf-8')))
    #file.close()


if __name__ == "__main__":
    main()
