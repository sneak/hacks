#!/usr/bin/env python3
from twitter_scraper import get_tweets

def main():
    for tweet in get_tweets('twitter', pages=1):
        print(tweet['username'], ": ", tweet['text'])

if __name__ == "__main__":
    main()
