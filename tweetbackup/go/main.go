package main

import (
	"encoding/json"
	"fmt"

	twitterscraper "github.com/n0madic/twitter-scraper"
)

func main() {
	profile, err := twitterscraper.GetProfile("jack")
	if err != nil {
		panic(err)
	}
	prettyJSON, err := json.MarshalIndent(profile, "", "    ")
	fmt.Printf("%s\n", string(prettyJSON))
}
