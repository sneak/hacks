#!/bin/bash
# Prevent *.sh from returning itself if there are no matches
shopt -s nullglob

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "waiting for network to come up..."

until ping -c1 -w1 www.google.com >/dev/null 2>&1; do :; done

echo "network is now up, running scripts"

# Run every per-once script
for FN in /boot/per-once.d/*.sh ; do
    echo "running '$FN'"
    bash "$FN" && \
        echo "renaming $FN" && \
        mv $FN $(dirname $FN)/$(basename $FN .sh).$(date +%F.%H.%M.%S)
done

# Run every per-boot script
for FN in /boot/per-boot.d/*.sh ; do
    echo "running '$FN'"
    bash "$FN"
done



