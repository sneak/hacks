#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt update && apt install -y curl

KEY_URL="https://sneak.cloud/authorized_keys"

# CAUTION! /!\

# doing only passwd -d <user> means they can log in at the console
# with no password whatsoever - probably not what you want

curl -fLo /root/.ssh/authorized_keys --create-dirs $KEY_URL && \
    passwd -d root && \
    passwd -l root

curl -fLo /home/pi/.ssh/authorized_keys --create-dirs $KEY_URL && \
    passwd -d pi && \
    passwd -l pi

chown -R pi:pi /home/pi/.ssh
