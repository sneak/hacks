#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PKGS="$(cat $DIR/packagelist.txt)"

export DEBIAN_FRONTEND=noninteractive

apt update

apt upgrade -y

apt install -y $PKGS
