#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt update
apt -y install ntpdate
ntpdate time.apple.com
