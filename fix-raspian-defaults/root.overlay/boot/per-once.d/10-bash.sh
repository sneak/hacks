#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

# install bash and only bash so that the setup scripts can assume bash
apt update
apt install -y bash
