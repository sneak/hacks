#!/bin/sh  # can't assume bash yet

if [ ! -d /boot/root.overlay ]; then
    logger -s -t rc.local.txt cant find root overlay in /boot
else
    rsync -avP /boot/root.overlay/ /
    ln -s /etc/systemd/system/raspberrypi-initial-setup.service \
        /etc/systemd/system/multi-user.target.wants/raspberrypi-initial-setup.service
    # we only need to run once
    rm -rf /boot/rc.local.txt /boot/root.overlay

    # after the above rsync has replaced the init_resize.sh with our
    # more sane version (it only doubles the size of the root, not
    # consuming the entire sd card), set the kernel command line to use the
    # init resizer on the next boot (like raspbian normally does)

    # this will only run this once, as we have already deleted ourselves in
    # the previous command.
    sed -i 's|$| init=/usr/lib/raspi-config/init_resize.sh|' /boot/cmdline.txt

fi

reboot
