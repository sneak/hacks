#!/bin/bash

SSH="ssh root@las1"
$SSH virsh pool-define-as flashpool-root dir --target /srv/flash/virt/root
$SSH virsh pool-start flashpool-root
$SSH virsh pool-autostart flashpool-root
$SSH virsh pool-define-as flashpool-storage dir --target /srv/flash/virt/storage
$SSH virsh pool-start flashpool-storage
$SSH virsh pool-autostart flashpool-storage
$SSH virsh pool-define-as pool-root dir --target /srv/storage/virt/root
$SSH virsh pool-start pool-root
$SSH virsh pool-autostart pool-root
$SSH virsh pool-define-as pool-storage dir --target /srv/storage/virt/storage
$SSH virsh pool-start pool-storage
$SSH virsh pool-autostart pool-storage
$SSH virsh pool-list

SSH="ssh root@lstor1"
$SSH virsh pool-define-as pool-root dir --target /srv/lstor1/virt/root
$SSH virsh pool-start pool-root
$SSH virsh pool-autostart pool-root
$SSH virsh pool-define-as pool-storage dir --target /srv/lstor1/virt/storage
$SSH virsh pool-start pool-storage
$SSH virsh pool-autostart pool-storage
$SSH virsh pool-list
