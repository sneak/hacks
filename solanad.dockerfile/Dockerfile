FROM ubuntu:focal as builder

ENV DEBIAN_FRONTEND noninteractive

# this is the hash of solana-1.3.14
ENV VERSION c8f4bfca90e5087f77dd11921b75231c5fa30151


RUN apt update && apt install -y \
    clang \
    cmake \
    curl \
    git \
    golang \
    lcov \
    libssl-dev \
    libudev-dev \
    llvm \
    mscgen \
    net-tools \
    pkg-config \
    rsync \
    sudo \
    zlib1g-dev \
    unzip

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
RUN . $HOME/.cargo/env && rustup component add rustfmt && rustup update

RUN git clone \
    https://github.com/solana-labs/solana.git \
    /usr/local/src/solana && \
    cd /usr/local/src/solana && \
    git checkout $VERSION

WORKDIR /usr/local/src/solana

RUN . $HOME/.cargo/env && cargo build --release

# kept crashing docker desktop, hard locks on OOM instead of doing
# something sensible. fuck closed source, proprietary software.
#RUN . $HOME/.cargo/env && cargo cargo test
#RUN . $HOME/.cargo/env rustup install nightly && cargo +nightly bench

RUN cd target/release && \
    rm -f *.so *.d *.rlib && \
    mv -v * /usr/local/bin

FROM ubuntu:focal as runner
COPY --from=builder /usr/local/bin/* /usr/local/bin/

ENV DEBIAN_FRONTEND noninteractive
ENV PATH $PATH:/usr/local/bin

RUN apt update && apt install -y \
    runit \
    runit-systemd

COPY ./supervise.sh /usr/local/sbin/supervise.sh
RUN mkdir -p /etc/service/solanad
COPY ./run.solanad /etc/service/solanad/run
RUN chmod +x /etc/service/*/run

#RUN solana transaction-count
#RUN solana-gossip spy --entrypoint devnet.solana.com:8001
RUN mkdir -p /var/lib/solana && chown nobody:nogroup /var/lib/solana
ENV HOME /var/lib/solana
WORKDIR /var/lib/solana
USER nobody:nogroup

ENV NETWORK_URL https://devnet.solana.com
#RUN solana config set --url http://devnet.solana.com

VOLUME /var/lib/solana
# rpc
EXPOSE 8899

# this daemon is stupid
EXPOSE 8000
EXPOSE 8001
EXPOSE 8002
EXPOSE 8003
EXPOSE 8004
EXPOSE 8005
EXPOSE 8005
EXPOSE 8006
EXPOSE 8007
EXPOSE 8008
EXPOSE 8009
EXPOSE 8010

USER root
WORKDIR /
CMD [ "/bin/sh", "/usr/local/sbin/supervise.sh" ]
