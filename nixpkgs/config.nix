{
  allowUnsupportedSystem = true;
  packageOverrides = pkgs: with pkgs; rec {
    myProfile = writeText "my-profile" ''
      export PATH=$HOME/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/sbin:/bin:/usr/sbin:/usr/bin
      export MANPATH=$HOME/.nix-profile/share/man:/nix/var/nix/profiles/default/share/man:/usr/share/man
    '';
    nostromo = pkgs.buildEnv {
      name = "nostromopackages";
      paths = [
        basePackages
        macPackages
      ];
      pathsToLink = [ "/Applications" "/share/man" "/share/doc" "/bin" "/etc" ];
      extraOutputsToInstall = [ "man" "doc" ];
    };
    secondmillion = pkgs.buildEnv {
      name = "secondmillion";
      paths = [
        basePackages
        macPackages
      ];
      pathsToLink = [ "/Applications" "/share/man" "/share/doc" "/bin" "/etc" ];
      extraOutputsToInstall = [ "man" "doc" ];
    };
    macPackages = pkgs.buildEnv {
      name = "macPackages";
      paths = [
        pinentry_mac
      ];
      pathsToLink = [ "/Applications" "/share/man" "/share/doc" "/bin" "/etc" ];
      extraOutputsToInstall = [ "man" "doc" ];
    };
    adminPackages = pkgs.buildEnv {
      name = "adminPackages";
      paths = [
        (runCommand "profile" {} ''
          mkdir -p $out/etc/profile.d
          cp ${myProfile} $out/etc/profile.d/my-profile.sh
        '')
        byobu
        envdir
        httpie
        jq
        mosh
        nmap
        openssl
        pv
        rsync
        runit
        terraform
        tmux
        wget
        xz
      ];
      pathsToLink = [ "/share/man" "/share/doc" "/bin" "/etc" ];
      extraOutputsToInstall = [ "man" "doc" ];

    };
    basePackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [
        (runCommand "profile" {} ''
          mkdir -p $out/etc/profile.d
          cp ${myProfile} $out/etc/profile.d/my-profile.sh
        '')
        adminPackages
        aria2
        byobu
        cmus
        coreutils-prefixed
        envdir
        ffmpeg
        gnupg
        go
        httpie
        jq
        neofetch
        neovim
        nmap
        nodejs
        openssl
        par2cmdline
        pv
        pwgen
        rsync
        runit
        tmux
        tor
        vim
        vimpager
        weechat
        wget
        xz
        yarn
      ];
      pathsToLink = [ "/share/man" "/share/doc" "/bin" "/etc" ];
      extraOutputsToInstall = [ "man" "doc" ];

    };
  };
}
