#!/bin/bash

DST="root@lstor1.local:/srv/lstor1/backup/temp-nostromo-migration"
SRC=""

sudo rsync -avP \
    --delete --delete-excluded \
    --exclude /proc \
    --exclude /dev \
    --exclude /.fseventsd \
    --exclude /private/var/vm \
    --exclude /System/Volumes/Data/Volumes \
    --exclude /System/Volumes/Data/nix \
    --exclude /System/Volumes/Data/.fseventsd \
    --exclude /System/Volumes/Data/.Spotlight-V100 \
    --exclude /System/Volumes/Data/Users/sneak/.Trash \
    --exclude /System/Volumes/Data/Users/sneak/.cache \
    --exclude /System/Volumes/Data/Users/sneak/Library/Caches \
    --exclude /System/Volumes/Data/private/var/folders \
    --exclude /Volumes \
    "$SRC"/ "$DST"/
