#!/usr/bin/env zx

const api = "https://lightning.ambientweather.net/devices";
const outputDir = `${process.env.HOME}/tmp/weatherfetch`;
const queryString =
    "%24publicBox%5B0%5D%5B0%5D=-115.37389456264378&%24publicBox%5B0%5D%5B1%5D=36.1098453902911&%24publicBox%5B1%5D%5B0%5D=-115.1709572152316&%24publicBox%5B1%5D%5B1%5D=36.24422733946878&%24limit=500";
const userAgent =
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36";

async function fetchWeather() {
    const cw = await $`curl ${api}?${queryString} \
        -H 'accept: application/json' \
        -H 'authority: lightning.ambientweather.net' \
        -H 'user-agent: '${userAgent} \
        -H 'origin: https://ambientweather.net' \
        -H 'sec-fetch-site: same-site' \
        -H 'sec-fetch-mode: cors' \
        -H 'sec-fetch-dest: empty' \
        -H 'referer: https://ambientweather.net/' \
        -H 'accept-language: en-US,en;q=0.9' \
        --compressed 2>/dev/null`;
    const o = await JSON.parse(cw);
    return o;
}

function minToMs(minutes) {
    return minutes * 60 * 1000;
}

async function main() {
    await $`test -d ${outputDir} || mkdir -p ${outputDir}`;
    while (true) {
        await oneLoop();
        await sleep(minToMs(30));
    }
}

async function oneLoop() {
    const now = await $`date -u +"%Y%m%dT%H%M%SZ"`;
    const data = await fetchWeather();

    fs.writeFileSync(outputDir + "/latest.json", JSON.stringify(data));
    await $`cp ${outputDir}/latest.json ${outputDir}/${now}.json`;
}

main();
