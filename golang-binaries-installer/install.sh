# this is designed to be symlinked to ~/.bashrc.d/999.goinstaller.sh

if ! which mkcert 2>&1 >/dev/null ; then
    go install -v filippo.io/mkcert@latest
fi

if ! which certstrap 2>&1 >/dev/null ; then
    go install -v github.com/square/certstrap@latest
fi

if ! which f2 2>&1 >/dev/null ; then
    go install -v github.com/ayoisaiah/f2/cmd/f2@latest
fi

if ! which pack 2>&1 >/dev/null ; then
    go install -v github.com/buildpacks/pack/cmd/pack@latest
fi

if ! which golangci-lint 2>&1 >/dev/null ; then
    go install -v github.com/golangci/golangci-lint/cmd/golangci-lint@latest
fi

if ! which gofumpt 2>&1 >/dev/null ; then
    go install -v mvdan.cc/gofumpt@latest
fi

if ! which countdown 2>&1 >/dev/null ; then
    go install -v github.com/antonmedv/countdown@latest
fi
