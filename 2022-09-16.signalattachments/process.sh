#!/bin/bash


for FN in $(file * | grep -i png | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.png
done
for FN in $(file * | grep -i jpeg | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.jpg
done
for FN in $(file * | grep -i gif | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.gif
done
for FN in $(file * | grep -i 'Web/P' | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.webp
done
for FN in $(file * | grep -i mp4 | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.mp4
done
for FN in $(file * | grep -i pdf | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.pdf
done
for FN in $(file * | grep -i "\.M4A" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.m4a
done
for FN in $(file * | grep "MPEG ADTS, AAC" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.aac
done
for FN in $(file * | grep "EPUB" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.epub
done
for FN in $(file * | grep "Zip archive" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.zip
done
for FN in $(file * | grep "Unicode text" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.txt
done
for FN in $(file * | grep "ASCII text" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.txt
done
for FN in $(file * | grep "empty" | awk -F':' '{print $1}') ; do
    rm -v $FN
done
for FN in $(file * | grep "data" | awk -F':' '{print $1}') ; do
    mv -v $FN ../out/$FN.dat
done

exit 1

fdupes -d -q -N ../out

f2 -r '{{mtime.YYYY}}-{{mtime.MM}}/{{mtime.YYYY}}-{{mtime.MM}}-{{mtime.DD}}.{{f}}{{ext}}' -x
