#!/usr/bin/env python3
import time
import termgraphics

#g = termgraphics.TermGraphics(mode = termgraphics.MODE_EASCII)
g = termgraphics.TermGraphics()

g.clear()

for x in range(g.shape[0]):
    y = int(0.5 * g.shape[1] * ((2*x/g.shape[0]-1)**3 + 1))
    g.point((x,y))

    g.draw()
    time.sleep(0.1)

